//
//  ViewController.m
//  ColorMixer
//
//  Created by Ebba on 2016-01-28.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (weak, nonatomic) IBOutlet UIView *colorResult;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.colorResult.backgroundColor = [self mixedColor];
}

-(UIColor*)mixedColor{
    return [UIColor colorWithRed:self.redSlider.value
                           green:self.greenSlider.value
                            blue:self.blueSlider.value
                           alpha:1.0f];
}
- (IBAction)sliderChanged:(UISlider *)sender {
    self.colorResult.backgroundColor = [self mixedColor];
}
- (IBAction)resetColor:(UIButton *)sender {
    
    self.redSlider.value = 0.5;
    self.blueSlider.value = 0.5;
    self.greenSlider.value = 0.5;
    self.colorResult.backgroundColor = [self mixedColor];
}

@end
